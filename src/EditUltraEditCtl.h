#ifndef _H_EDITULTRA_WINDOW_
#define _H_EDITULTRA_WINDOW_

#include "framework.h"

int CreateScintillaControl( struct TabPage *pnodeTabPage );
void DestroyScintillaControl( struct TabPage *pnodeTabPage );

int InitTabPageControlsCommonStyle( struct TabPage *pnodeTabPage );
int InitTabPageControlsBeforeLoadFile( struct TabPage *pnodeTabPage );
int InitTabPageControlsAfterLoadFile( struct TabPage *pnodeTabPage );
int CleanTabPageControls( struct TabPage *pnodeTabPage );

void GetTextByRange( struct TabPage *pnodeTabPage , size_t start , size_t end , char *text );
void GetTextByLine( struct TabPage *pnodeTabPage , size_t nLineNo , char *acText , size_t nTextBufsize );

bool IsDocumentModified( struct TabPage *pnodeTabPage );

int QueryIndexFromTabPage( struct TabPage *pnodeTabPage );

int OnSavePointReached( struct TabPage *pnodeTabPage );
int OnSavePointLeft( struct TabPage *pnodeTabPage );
int OnMarginClick( struct TabPage *pnodeTabPage , SCNotification *lpnotify );
int OnCharAdded( struct TabPage *pnodeTabPage , SCNotification *lpnotify );
int OnUpdateUI( struct TabPage *pnodeTabPage );

int AutosetLineNumberMarginWidth( struct TabPage *pnodeTabPage );

#endif
