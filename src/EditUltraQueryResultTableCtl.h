#ifndef _H_EDITULTRA_QUERYRESULTTABLECTL_
#define _H_EDITULTRA_QUERYRESULTTABLECTL_

#include "framework.h"

/*
 * 结果表格控件
 */

int CreateQueryResultTableCtl( struct TabPage *pnodeTabPage , HFONT hFont );

/*
 * SQL
 */

#define SQL_WORD2_BUFFER_SIZE		10*1024*1024

typedef MYSQL *func_mysql_init( MYSQL *mysql );
typedef MYSQL *func_mysql_real_connect( MYSQL *mysql , char *dbhost , char *dbuser , char *dbpass , char *dbname , unsigned int dbport , const char *unix_socket , unsigned long clientflag ) ;
typedef int func_mysql_set_character_set( MYSQL *mysql , char *encoding );
typedef int func_mysql_query( MYSQL *mysql , char *sql );
typedef my_ulonglong func_mysql_affected_rows( MYSQL *mysql );
typedef MYSQL_RES *func_mysql_store_result( MYSQL *mysql );
typedef unsigned int func_mysql_num_fields( MYSQL_RES *res );
typedef MYSQL_FIELD *func_mysql_fetch_field( MYSQL_RES *res );
typedef MYSQL_ROW func_mysql_fetch_row( MYSQL_RES *res );
typedef void func_mysql_free_result( MYSQL_RES *res );
typedef MYSQL *func_mysql_close( MYSQL *mysql );

struct MySqlFunctions
{
	HMODULE				hmod_libmysql_dll ;
	func_mysql_init			*pfunc_mysql_init ;
	func_mysql_real_connect		*pfunc_mysql_real_connect ;
	func_mysql_set_character_set	*pfunc_mysql_set_character_set ;
	func_mysql_query		*pfunc_mysql_query ;
	func_mysql_affected_rows	*pfunc_mysql_affected_rows ;
	func_mysql_store_result		*pfunc_mysql_store_result ;
	func_mysql_num_fields		*pfunc_mysql_num_fields ;
	func_mysql_fetch_field		*pfunc_mysql_fetch_field ;
	func_mysql_fetch_row		*pfunc_mysql_fetch_row ;
	func_mysql_free_result		*pfunc_mysql_free_result ;
	func_mysql_close		*pfunc_mysql_close ;
} ;

typedef sword funcOCIEnvCreate(OCIEnv **envp, ub4 mode, void  *ctxp, void  *(*malocfp)(void  *ctxp, size_t size), void  *(*ralocfp)(void  *ctxp, void  *memptr, size_t newsize), void   (*mfreefp)(void  *ctxp, void  *memptr),	size_t xtramem_sz, void  **usrmempp);
typedef sword funcOCIHandleAlloc(const void  *parenth, void  **hndlpp, const ub4 type, const size_t xtramem_sz, void  **usrmempp);
typedef sword funcOCIHandleFree(void  *hndlp, const ub4 type);
typedef sword funcOCIServerAttach(OCIServer *srvhp, OCIError *errhp, const OraText *dblink, sb4 dblink_len, ub4 mode);
typedef sword funcOCIServerDetach(OCIServer *srvhp, OCIError *errhp, ub4 mode);
typedef sword funcOCISessionBegin(OCISvcCtx *svchp, OCIError *errhp, OCISession *usrhp, ub4 credt, ub4 mode);
typedef sword funcOCISessionEnd(OCISvcCtx *svchp, OCIError *errhp, OCISession *usrhp,  ub4 mode);
typedef sword funcOCIAttrGet(const void  *trgthndlp, ub4 trghndltyp, void  *attributep, ub4 *sizep, ub4 attrtype, OCIError *errhp);
typedef sword funcOCIAttrSet(void  *trgthndlp, ub4 trghndltyp, void  *attributep, ub4 size, ub4 attrtype, OCIError *errhp);
typedef sword funcOCIParamGet(const void  *hndlp, ub4 htype, OCIError *errhp, void  **parmdpp, ub4 pos);
typedef sword funcOCIParamSet(void  *hdlp, ub4 htyp, OCIError *errhp, const void  *dscp, ub4 dtyp, ub4 pos);
typedef sword funcOCIStmtPrepare(OCIStmt *stmtp, OCIError *errhp, const OraText *stmt, ub4 stmt_len, ub4 language, ub4 mode);
typedef sword funcOCIStmtPrepare2( OCISvcCtx *svchp, OCIStmt **stmtp, OCIError *errhp, const OraText *stmt, ub4 stmt_len, const OraText *key, ub4 key_len, ub4 language, ub4 mode);
typedef sword funcOCIDefineByPos(OCIStmt *stmtp, OCIDefine **defnp, OCIError *errhp, ub4 position, void  *valuep, sb4 value_sz, ub2 dty, void  *indp, ub2 *rlenp, ub2 *rcodep, ub4 mode);
typedef sword funcOCIDefineByPos2(OCIStmt *stmtp, OCIDefine **defnp, OCIError *errhp, ub4 position, void  *valuep, sb8 value_sz, ub2 dty, void  *indp, ub4 *rlenp, ub2 *rcodep, ub4 mode);
typedef sword funcOCIStmtExecute(OCISvcCtx *svchp, OCIStmt *stmtp, OCIError *errhp, ub4 iters, ub4 rowoff, const OCISnapshot *snap_in, OCISnapshot *snap_out, ub4 mode);
typedef sword funcOCIStmtFetch(OCIStmt *stmtp, OCIError *errhp, ub4 nrows, ub2 orientation, ub4 mode);
typedef sword funcOCIStmtFetch2(OCIStmt *stmtp, OCIError *errhp, ub4 nrows, ub2 orientation, sb4 scrollOffset, ub4 mode);
typedef sword funcOCIErrorGet(void  *hndlp, ub4 recordno, OraText *sqlstate, sb4 *errcodep, OraText *bufp, ub4 bufsiz, ub4 type);
typedef sword funcOCITransStart(OCISvcCtx *svchp, OCIError *errhp, uword timeout, ub4 flags );
typedef sword funcOCITransCommit(OCISvcCtx *svchp, OCIError *errhp, ub4 flags);
typedef sword funcOCITransRollback(OCISvcCtx *svchp, OCIError *errhp, ub4 flags);
typedef sword funcOCITransDetach(OCISvcCtx *svchp, OCIError *errhp, ub4 flags );


struct OracleFunctions
{
	HMODULE				hmod_oci_dll ;
	funcOCIEnvCreate		*pfuncOCIEnvCreate ;
	funcOCIHandleAlloc		*pfuncOCIHandleAlloc ;
	funcOCIHandleFree		*pfuncOCIHandleFree ;
	funcOCIServerAttach		*pfuncOCIServerAttach ;
	funcOCIServerDetach		*pfuncOCIServerDetach ;
	funcOCISessionBegin		*pfuncOCISessionBegin ;
	funcOCISessionEnd		*pfuncOCISessionEnd ;
	funcOCIAttrGet			*pfuncOCIAttrGet ;
	funcOCIAttrSet			*pfuncOCIAttrSet ;
	funcOCIParamGet			*pfuncOCIParamGet ;
	funcOCIParamSet			*pfuncOCIParamSet ;
	funcOCIStmtPrepare		*pfuncOCIStmtPrepare ;
	funcOCIStmtPrepare2		*pfuncOCIStmtPrepare2 ;
	funcOCIDefineByPos		*pfuncOCIDefineByPos ;
	funcOCIDefineByPos2		*pfuncOCIDefineByPos2 ;
	funcOCIStmtExecute		*pfuncOCIStmtExecute ;
	funcOCIStmtFetch		*pfuncOCIStmtFetch ;
	funcOCIStmtFetch2		*pfuncOCIStmtFetch2 ;
	funcOCIErrorGet			*pfuncOCIErrorGet ;
	funcOCITransStart		*pfuncOCITransStart ;
	funcOCITransCommit		*pfuncOCITransCommit ;
	funcOCITransRollback		*pfuncOCITransRollback ;
	funcOCITransDetach		*pfuncOCITransDetach ;
} ;

typedef SQLITE_API int func_sqlite3_open(
	const char *filename,   /* Database filename (UTF-8) */
	sqlite3 **ppDb          /* OUT: SQLite db handle */
);
typedef SQLITE_API int func_sqlite3_exec(
	sqlite3*,                                  /* An open database */
	const char *sql,                           /* SQL to be evaluated */
	int (*callback)(void*,int,char**,char**),  /* Callback function */
	void *,                                    /* 1st argument to callback */
	char **errmsg                              /* Error msg written here */
);
typedef SQLITE_API int func_sqlite3_get_table(
	sqlite3 *db,          /* An open database */
	const char *zSql,     /* SQL to be evaluated */
	char ***pazResult,    /* Results of the query */
	int *pnRow,           /* Number of result rows written here */
	int *pnColumn,        /* Number of result columns written here */
	char **pzErrmsg       /* Error msg written here */
);
typedef SQLITE_API void func_sqlite3_free_table(char **result);
typedef SQLITE_API int func_sqlite3_close(sqlite3*);;

struct Sqlite3Functions
{
	HMODULE				hmod_sqlite3_dll ;
	func_sqlite3_open		*pfunc_sqlite3_open ;
	func_sqlite3_exec		*pfunc_sqlite3_exec ;
	func_sqlite3_get_table		*pfunc_sqlite3_get_table ;
	func_sqlite3_free_table		*pfunc_sqlite3_free_table ;
	func_sqlite3_close		*pfunc_sqlite3_close ;
};

typedef PGconn *funcPQsetdbLogin(const char *pghost, const char *pgport,
	const char *pgoptions, const char *pgtty,
	const char *dbName,
	const char *login, const char *pwd);
typedef void funcPQfinish(PGconn *conn);
typedef PGresult *funcPQexec(PGconn *conn, const char *query);
typedef char *funcPQcmdTuples(PGresult *res);
typedef int funcPQntuples(const PGresult *res);
typedef int funcPQnfields(const PGresult *res);
typedef char *funcPQfname(const PGresult *res, int field_num);
typedef char *funcPQgetvalue(const PGresult *res, int tup_num, int field_num);
typedef int funcPQgetisnull(const PGresult *res, int tup_num, int field_num);
typedef void funcPQclear(PGresult *res);
typedef ExecStatusType funcPQresultStatus(const PGresult *res);
typedef char *funcPQresultErrorMessage(const PGresult *res);
typedef int funcPQsetClientEncoding(PGconn *conn, const char *encoding);

struct PostgreSQLFunctions
{
	HMODULE				hmod_libpq_dll ;
	funcPQsetdbLogin		*pfuncPQsetdbLogin ;
	funcPQfinish			*pfuncPQfinish ;
	funcPQexec			*pfuncPQexec ;
	funcPQcmdTuples			*pfuncPQcmdTuples ;
	funcPQntuples			*pfuncPQntuples ;
	funcPQnfields			*pfuncPQnfields ;
	funcPQfname			*pfuncPQfname ;
	funcPQgetvalue			*pfuncPQgetvalue ;
	funcPQgetisnull			*pfuncPQgetisnull ;
	funcPQclear			*pfuncPQclear ;
	funcPQresultStatus		*pfuncPQresultStatus ;
	funcPQresultErrorMessage	*pfuncPQresultErrorMessage ;
	funcPQsetClientEncoding		*pfuncPQsetClientEncoding ;
};

struct DatabaseLibraryFunctions
{
	struct MySqlFunctions		stMysqlFunctions ;
	struct OracleFunctions		stOracleFunctions ;
	struct Sqlite3Functions		stSqlite3Functions ;
	struct PostgreSQLFunctions	stPostgreSQLFunctions ;
};

extern struct DatabaseLibraryFunctions		stDatabaseLibraryFunctions ;

struct DatabaseConnectionConfig
{
	char	dbtype[ 16 ] ;
	char	dbhost[ 256 ] ;
	int	dbport ;
	char	dbuser[ 64 ] ;
	char	dbpass[ 64 ] ;
	char	dbname[ 64 ] ;

	BOOL	bConfigDbPass ;
};

struct MySqlHandles
{
	MYSQL		*mysql ;
} ;

struct OracleHandles
{
	OCIEnv		*envhpp ;
	OCIServer	*servhpp ;
	OCIError	*errhpp ;
	OCISession	*usrhpp ;
	OCISvcCtx	*svchpp ;
} ;

struct Sqlite3Handles
{
	sqlite3		*sqlite3 ;
};

struct PostgreSQLHandles
{
	PGconn		*postgres ;
};

struct DatabaseConnectionHandles
{
	union ConnectionHandles
	{
		struct MySqlHandles		stMysqlHandles ;
		struct OracleHandles		stOracleHandles ;
		struct Sqlite3Handles		stSqlite3Handles ;
		struct PostgreSQLHandles	stPostgreSQLHandles ;
	} handles ;
};

int GetOracleErrCode( struct TabPage *pnodeTabPage , OCIError *errhpp , int *pnErrorCode , char *pcErrorDesc , size_t nErrorDescBufsize );
int DisconnectFromDatabase( struct TabPage *pnodeTabPage );
int ConnectToDatabase( struct TabPage *pnodeTabPage );
int ExecuteSqlQuery( struct TabPage *pnodeTabPage );

#endif
