#ifndef _H_EDITULTRA_UTIL_
#define _H_EDITULTRA_UTIL_

#include "framework.h"

#ifndef MAX
#define MAX(_a_,_b_) ((_a_)>(_b_)?(_a_):(_b_))
#endif

#ifndef MIN
#define MIN(_a_,_b_) ((_a_)<(_b_)?(_a_):(_b_))
#endif

void GenarateConfigKey();
int AesEncrypt( unsigned char *dec , unsigned char *enc , int len );
int AesDecrypt( unsigned char *enc , unsigned char *dec , int len );

int Encrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len );
int Decrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len );
int Encrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len
	, unsigned char *init_vector );
int Decrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len
	, unsigned char *init_vector );

int HexExpand( char *HexBuf , int HexBufLen , char *AscBuf );
int HexFold( char *AscBuf , int AscBufLen , char *HexBuf );

enum FileType
{
	FILETYPE_ERROR = -1 ,
	FILETYPE_REGULAR = 1 ,
	FILETYPE_DIRECTORY = 2 ,
	FILETYPE_OTHER = 3
};

enum FileType GetFileType( char *acPathFileName );

char **CommandLineToArgvA( LPWSTR lpCmdLine, int *_argc );

int SetWindowTitle( char* filename );
BOOL CenterWindow(HWND hWnd, HWND hParent);
char *StrdupEditorSelection( size_t *p_nTextLen , size_t multiple );
char *StrdupUtf8ToGb( char *utf8_text );
char *StrdupGbToUtf8( char *gb_text );
char *GetFontNameUtf8FromGb( char *fontname_gb );
void CopyEditorSelectionToWnd( struct TabPage *pnodeTabPage , HWND hwnd );

void SetMenuItemEnable(HWND hWnd, int nMenuItemId, bool enable);
void SetMenuItemChecked(HWND hWnd, int nMenuItemId, bool checked);

void InfoBox( const char *format , ... );
void WarnBox( const char *format , ... );
void ErrorBox( const char *format , ... );

char* ConvertEncodingEx( const char *encFrom , const char *encTo , const char *p_ori_in , size_t ori_in_len , char *p_ori_out , size_t *p_ori_out_size );

void FoldNewLineString( char *str );

void ToUpperString( char *str );
char *strword( char *str , char *word );

int GetApplicationByFileExt( char *acFileExtName , char *acApplicationName );
int SetApplicationByFileExt( char *acFileExtName , char *acApplicationName );

char *filedup( char *pathfilename , size_t *p_file_len );

#if 0
void HexByteFold( unsigned char leftchar , unsigned char rightchar , unsigned char *p_byte );
void HexByteExpand( unsigned char byte , unsigned char doublechar[2+1] );
#endif

#define HEXCHARSET	"0123456789ABCDEF"

#define HexByteFold(_leftchar_,_rightchar_,_byte_) \
	{ \
		unsigned char *p1 = (unsigned char *)strchr( HEXCHARSET , (int)(_leftchar_) ) ; \
		unsigned char *p2 = (unsigned char *)strchr( HEXCHARSET , (int)(_rightchar_) ) ; \
		if( p1 == NULL || p2 == NULL ) \
		{ \
			(_byte_) = '\0' ; \
		} \
		else \
		{ \
			(_byte_) = (unsigned char)((p1-(unsigned char*)HEXCHARSET)<<4) + (unsigned char)(p2-(unsigned char*)HEXCHARSET) ; \
		} \
	} \

#define HexByteExpand(_byte_,_doublechar_) \
	{ \
		(_doublechar_)[0] = HEXCHARSET[((_byte_)&0xF0)>>4] ; \
		(_doublechar_)[1] = HEXCHARSET[(_byte_)&0x0F] ; \
		(_doublechar_)[2] = '\0' ; \
	} \

#endif

int QueryNetIpByHostName( char *hostname , char *ip , int ip_bufsize );

int GetEditorEffectStartAndEndLine( struct TabPage *pnodeTabPage , int *p_nSelectStartLine , int *p_nSelectEndLine );
