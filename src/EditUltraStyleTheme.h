#ifndef _H_EDITULTRA_STYLETHEME_
#define _H_EDITULTRA_STYLETHEME_

#include "framework.h"

struct StyleClass
{
	char			font[ 32 ] ;
	int			fontsize ;
	int			color ;
	int			bgcolor ;
	BOOL			bold ;
};

struct StyleTheme
{
	struct StyleClass	linenumber ;
	struct StyleClass	foldmargin ;

	struct StyleClass	text ;
	struct StyleClass	caretline ;
	struct StyleClass	indicator ;

	struct StyleClass	keywords ;
	struct StyleClass	keywords2 ;
	struct StyleClass	string ;
	struct StyleClass	character ;
	struct StyleClass	number ;
	struct StyleClass	operatorr ;
	struct StyleClass	preprocessor ;
	struct StyleClass	comment ;
	struct StyleClass	commentline ;
	struct StyleClass	commentdoc ;

	struct StyleClass	tags ;
	struct StyleClass	unknowtags ;
	struct StyleClass	attributes ;
	struct StyleClass	unknowattributes ;
	struct StyleClass	entities ;
	struct StyleClass	tagends ;
	struct StyleClass	cdata ;
	struct StyleClass	phpsection ;
	struct StyleClass	aspsection ;
} ;

struct WindowTheme
{
	char			acPathFilename[ MAX_PATH ] ;
	char			acThemeName[ MAX_PATH ] ;
	struct StyleTheme	stStyleTheme ;
};

extern struct WindowTheme		g_astWindowTheme[ VIEW_STYLETHEME_MAXCOUNT ] ;
extern int				g_nWindowThemeCount ;
extern struct WindowTheme		*g_pstWindowTheme ;

extern HBRUSH				g_brushCommonControlDarkColor ;

/*
void SetStyleThemeDefault( struct StyleTheme *pstStyleTheme );
*/
int LoadStyleThemeConfigFile( struct WindowTheme *pstWindowTheme , char *filebuf );
void UpdateWindowThemeMenu();
int LoadAllStyleThemeConfigFiles( char *filebuf );
int SaveStyleThemeConfigFile();

int CopyNewThemeStyle( char *acStyleTheme );

INT_PTR CALLBACK StyleThemeWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

#endif
